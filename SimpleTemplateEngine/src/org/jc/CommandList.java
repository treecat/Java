package org.jc;

import java.util.List;


public class CommandList {
	
	private List<Command> commandList;

	public List<Command> getCommandList() {
		return commandList;
	}

	public void setCommandList(List<Command> commandList) {
		this.commandList = commandList;
	}
	
}
