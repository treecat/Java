package org.jc;

public class StringUtil {
	
	private static String[] SPECIAL_CHARARECTER = {"?",".","{","}"};
	
	public static String escape(String rawString) {
		
		String tem=rawString;
		
		for(int i=0;i<SPECIAL_CHARARECTER.length;i++) {
			
			tem = tem.replace(SPECIAL_CHARARECTER[i], "\\"+SPECIAL_CHARARECTER[i]);
		}
		return tem;
	}
}
