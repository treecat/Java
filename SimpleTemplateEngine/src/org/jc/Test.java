package org.jc;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.thoughtworks.xstream.XStream;

public class Test {

	public static void main(String[] args) {

		XStream x = new XStream();
		x.alias("commandList", CommandList.class);
		x.alias("command", Command.class);
		x.addImplicitCollection(CommandList.class, "commandList");
		/*ArrayList<Command> list = new ArrayList<Command>();

		CommandList clist = new CommandList();

		Command c1 = new Command();
		c1.setValue("ip address");
		c1.setExpected("[");
		c1.setSend("ye");

		Command c2 = new Command();
		c2.setValue("ip address");
		c2.setExpected("[");
		c2.setSend("ye");

		list.add(c1);
		list.add(c2);

		clist.setCommandList(list);
		String xmlStr = x.toXML(clist);*/

		CommandList t = (CommandList) x.fromXML(new File("test.xml"));

		Pattern p = Pattern.compile(StringUtil.escape(Constant.START_PLACEHOLDER)+ "(.+?)" + StringUtil.escape(Constant.END_PLACEHOLDER));

		Map<String,String> key = new HashMap<String,String>();
		key.put("ip", "12234");
		key.put("netmask", "255.255.255.0");
		key.put("gateway", "122.1.2.2");
		
		for (int i = 0; i < t.getCommandList().size(); i++) {

			Command c = t.getCommandList().get(i);

			if (c.getValue() != null) {
				
				System.out.println("raw String:"+ c.getValue());
				String tem = c.getValue();
				
				Matcher m = p.matcher(c.getValue());
				while(m.find()) {
					System.out.print(m.group(1));
					if (key.containsKey(m.group(1))) {
						System.out.print(" "+ key.get(m.group(1)) +'\n');
						tem = tem.replaceAll("("+StringUtil.escape(Constant.START_PLACEHOLDER)+ m.group(1) + StringUtil.escape(Constant.END_PLACEHOLDER)+")", key.get(m.group(1)));
					}
				}
				System.out.println("placehoder:"+tem);
				
			}
			System.out.println("=============");
		}

	}
}
