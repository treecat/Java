package org.jc;


public class Command {
	
	private String value;
	
	private String expected;
	
	private String send;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getExpected() {
		return expected;
	}

	public void setExpected(String expected) {
		this.expected = expected;
	}

	public String getSend() {
		return send;
	}

	public void setSend(String send) {
		this.send = send;
	}
	
	public String toString() {
		
		return "value:"+this.value+" expected:"+this.expected+" send:"+this.send;
	}
	
}
